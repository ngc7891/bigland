import { createApp, provide, h } from "vue";
import App from "./App.vue";

import { apolloClient } from "@/auth/client";
import { DefaultApolloClient } from "@vue/apollo-composable";

import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
// import { loadFonts } from "./plugins/webfontloader";

/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";

/* import specific icons */
import { fas } from "@fortawesome/free-solid-svg-icons";

/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

/* add icons to the library */
library.add(fas);


createApp(App)
  .provide(DefaultApolloClient, apolloClient)
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(router)
  .use(store)
  .use(vuetify)
  .mount("#app");
