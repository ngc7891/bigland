import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createHttpLink } from "apollo-link-http";

// Настройка инстанса graphql

// Инстанс кэша
const cache = new InMemoryCache();

//  Создаем ссылку на эндпоинт
const baseHttpLink = createHttpLink({
  uri: "https://api.frontend.test.it.bigland.ru/api/graphql",
});

export const apolloClient = new ApolloClient({
  cache,
  link: baseHttpLink,
  // connectToDevTools: false,
});
